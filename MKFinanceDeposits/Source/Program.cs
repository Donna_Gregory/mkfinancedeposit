﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Threading;

namespace MKFinanceDeposit
{
    
    class Program
    {
        //string pathName = "K:\MCAKFinance\";

        static void Main(string[] args)
        {
            
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["BillingCS"].ConnectionString);            
            cn.Open();

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("GetUnreportedDeposits", cn);
                DataTable dt = new DataTable("Results");
                da.Fill(dt);

                char pad = ' ';
                char zeropad = '0';
                string DepositID = string.Empty;
                string MasterAccountID = string.Empty;
                decimal DepositTotal = 0;
                decimal RunningTotal = 0;
                DateTime depositdate = DateTime.MinValue;
                DateTime CurrentDate = DateTime.Now.Date;

                if (dt.Rows.Count > 0)
                {

                    // create a writer and open the file
                    TextWriter tw = new StreamWriter(@"\\swtmkapp01\d$\Transfer Files\MKFinanceDeposit\MKDeposit.txt");

                    foreach (DataRow dr in dt.Rows)
                    {
                        string row = string.Empty;

                        depositdate = DateTime.Parse(dr["DepositDate"].ToString());

                        //If this is a credit receipt, list all credits
                        if (dr["PlanCode"].ToString() == "")
                        {
                            DataTable dtCredit = new DataTable();
                            dtCredit = getCreditTransactions(long.Parse(dr["ReceiptID"].ToString()), cn);

                            foreach (DataRow drCredit in dtCredit.Rows)
                            {
                                row = "6010000K";
                                row += depositdate.Month.ToString().PadLeft(2, zeropad);
                                row += depositdate.Day.ToString().PadLeft(2, zeropad);
                                row += depositdate.Year.ToString().Substring(2, 2);
                                row += CurrentDate.Month.ToString().PadLeft(2, zeropad);
                                row += CurrentDate.Day.ToString().PadLeft(2, zeropad);
                                row += CurrentDate.Year.ToString().Substring(2, 2);
                                row += "MCK ";
                                row += dr["AccountID"].ToString();
                                row += Math.Abs(decimal.Parse(drCredit["TransactionAmount"].ToString())).ToString("###0.00").Replace(".", "").PadLeft(9, zeropad);
                                if (decimal.Parse(drCredit["TransactionAmount"].ToString()) < 0)
                                {
                                    row += "D";
                                }
                                else
                                {
                                    row += "C";
                                }
                                row += dr["ReceiptID"].ToString().PadLeft(6, zeropad).PadRight(19, pad);
                                row += drCredit["PlanCode"].ToString().PadLeft(13, pad);
                                row += drCredit["OAStateCode"].ToString().PadLeft(5, zeropad).PadLeft(38, pad);
                                row = row.PadRight(120, pad);

                                tw.WriteLine(row.ToString());

                                RunningTotal += decimal.Parse(drCredit["TransactionAmount"].ToString());
                            }

                            dtCredit.Dispose();

                        }
                        else
                        {
                            row = "6010000K";
                            row += depositdate.Month.ToString().PadLeft(2, zeropad);
                            row += depositdate.Day.ToString().PadLeft(2, zeropad);
                            row += depositdate.Year.ToString().Substring(2, 2);
                            row += CurrentDate.Month.ToString().PadLeft(2, zeropad);
                            row += CurrentDate.Day.ToString().PadLeft(2, zeropad);
                            row += CurrentDate.Year.ToString().Substring(2, 2);
                            row += "MCK ";
                            row += dr["AccountID"].ToString();
                            row += Math.Abs(decimal.Parse(dr["ReceiptAmount"].ToString())).ToString("###0.00").Replace(".", "").PadLeft(9, zeropad);
                            if (decimal.Parse(dr["ReceiptAmount"].ToString()) < 0)
                            {
                                row += "D";
                            }
                            else
                            {
                                row += "C";
                            }
                            row += dr["ReceiptID"].ToString().PadLeft(6, zeropad).PadRight(19, pad);
                            row += dr["PlanCode"].ToString().PadLeft(13, pad);
                            row += dr["OAStateCode"].ToString().PadLeft(5, zeropad).PadLeft(38, pad);
                            row = row.PadRight(120, pad);

                            // write a line of text to the file
                            tw.WriteLine(row.ToString());

                            RunningTotal += decimal.Parse(dr["ReceiptAmount"].ToString());

                        }

                        DepositID = dr["DepositID"].ToString();
                        MasterAccountID = dr["MasterAccountID"].ToString();
                        DepositTotal = decimal.Parse(dr["DepositTotal"].ToString());

                    }


                    if (DepositTotal == RunningTotal)
                    {
                        string masterrow = string.Empty;
                        masterrow += "6010000K";
                        masterrow += depositdate.Month.ToString().PadLeft(2, zeropad);
                        masterrow += depositdate.Day.ToString().PadLeft(2, zeropad);
                        masterrow += depositdate.Year.ToString().Substring(2, 2);
                        masterrow += CurrentDate.Month.ToString().PadLeft(2, zeropad);
                        masterrow += CurrentDate.Day.ToString().PadLeft(2, zeropad);
                        masterrow += CurrentDate.Year.ToString().Substring(2, 2);
                        masterrow += "MCK ";
                        masterrow += MasterAccountID.ToString().PadRight(10, pad);
                        masterrow += Math.Abs(DepositTotal).ToString("###0.00").Replace(".", "").PadLeft(9, zeropad);
                        if (decimal.Parse(DepositTotal.ToString()) < 0)
                        {
                            masterrow += "C";
                        }
                        else
                        {
                            masterrow += "D";
                        }
                        masterrow = masterrow.PadRight(120, pad);

                        // write a line of text to the file
                        tw.WriteLine(masterrow.ToString());

                        // close the stream
                        tw.Close();

                        //Mark deposit record as ReportGenerated=True
                        UpdateDeposits(cn, DepositID);

                        sendEmail(DepositID, DepositTotal);

                    }
                    else
                    {
                        sendErrorEmail(DepositID, DepositTotal, RunningTotal);
                    }
                    sendConfirmationEmail("Deposits found.  MKDeposit file successfully created.");
                    Console.WriteLine("FILE CREATED");
                    Thread.Sleep(5000);
                }
                else
                {
                    sendConfirmationEmail("No deposits found.  File not created.");
                }
                da.Dispose();
                dt.Dispose();
            }
            catch(Exception ex)
            {
                sendErrorEmail(ex.ToString());
            }
            finally 
            {
                cn.Close();
            }
            
        }

        private static void UpdateDeposits(SqlConnection cn, string DepositID)
        {
            SqlCommand cmd = new SqlCommand("UpdateDepositForReportGenerated", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("DepositID", long.Parse(DepositID.ToString()));
            cmd.ExecuteNonQuery();
        }

        static DataTable getCreditTransactions(long ReceiptID, SqlConnection cn)
        {
            SqlDataAdapter da = new SqlDataAdapter("GetCreditTransactionsByReceiptID " + ReceiptID.ToString(), cn);
            DataTable dt = new DataTable("Results");
            da.Fill(dt);
          
            return dt;

        }

        static void sendEmail(string DepositID, decimal DepositAmount)
        {
            List<string> recipients = new List<string>();
            List<string> ccrecipients = new List<string>();
            List<string> bccrecipients = new List<string>();

            NotifyService.NotifyServiceClient email = new NotifyService.NotifyServiceClient();
            email.Open();

            StringBuilder MessageBody = new StringBuilder();
            MessageBody.Append("<table width='600' cellpadding='2'>");
            MessageBody.Append("<tr>");
            MessageBody.Append("<td class='highlightBodyHeader'>MKFinance Deposit File Notice</td>");
            MessageBody.Append("</tr>");
            MessageBody.Append("<tr>");
            MessageBody.Append("<td class='bodySmall'><br />NOTICE: A deposit file has been created from the MKFinance system for <b>DepositID " + DepositID.ToString() + "</b> in the amount of <b>" + DepositAmount.ToString("$#,##0.00") + "</b>.  This is for informational purposes only.<br /><br />");
            MessageBody.Append("Please contact <a href='mailto:rich.puskarich@oneamerica.com' class='linkSmall'>Jeff Haskins</a> if you have technical concerns regarding this file.</td>");
            MessageBody.Append("</tr>");
            MessageBody.Append("<tr>");
            MessageBody.Append("<td class='bodySmallBold'><br />McCready and Keene, Inc.<br /><br /><br /><br /></td>");
            MessageBody.Append("</tr>");
            MessageBody.Append("<tr>");
            MessageBody.Append("<td align='center'><a href='https://www.mcak.net/Finance/Reports/Deposits.aspx?DepositID=" + DepositID.ToString() + "' class='linkSmallBold'>Click here to view the details</a><br /><br /><br /><br /></td>");
            MessageBody.Append("</tr>");
            MessageBody.Append("</table>");

            recipients.Add("sharon.dave@oneamerica.com");
            recipients.Add("karla.remeika@oneamerica.com");
            recipients.Add("corporateaccounting.corpfin@oneamerica.com");
            ccrecipients.Add("jeremy.ehlerding@oneamerica.com");

            NotifyService.Msg message = new NotifyService.Msg();

            message.Recipients = recipients;
            message.CCRecipients = ccrecipients;
            message.BCCRecipients = bccrecipients;
            message.AppName = "MKFinance Deposit File Extract";
            message.isHTML = true;
            message.Sender = "notify@oneamerica.com";
            message.Subject = "MKFinance Deposit File";
            message.Message = MessageBody.ToString();

            email.SendMCAKFormattedEmail(message);

            email.Close();

        }

        static void sendErrorEmail(string DepositID, decimal DepositTotal, decimal RunningTotal)
        {
            
            NotifyService.NotifyServiceClient email = new NotifyService.NotifyServiceClient();
            email.Open();

            StringBuilder MessageBody = new StringBuilder();
            MessageBody.Append("<table width='600' cellpadding='2'>");
            MessageBody.Append("<tr>");
            MessageBody.Append("<td class='highlightBodyHeader'>MKFinance Deposit File - ERROR</td>");
            MessageBody.Append("</tr>");
            MessageBody.Append("<tr>");
            MessageBody.Append("<td class='bodySmall'><br />Error in creating the deposit file in the MKFinance system for <b>DepositID " + DepositID.ToString() + "</b>.<br /><br />Deposit Total from Deposit table:&nbsp;&nbsp;" + DepositTotal.ToString("$#,##0.00") + "<br />Running Total from sum of Receipt table:&nbsp;&nbsp;" + RunningTotal.ToString("$#,##0.00") + "<br /><br /><br /></td>");
            MessageBody.Append("</tr>");
            MessageBody.Append("</table>");
           
            NotifyService.Msg message = new NotifyService.Msg();

            message.Recipients = ConvertStringToStringList(System.Configuration.ConfigurationManager.AppSettings["TechnicalContacts"].ToString(), ';');
            message.AppName = "MKFinance Deposit File Extract";
            message.isHTML = true;
            message.Sender = "error@oneamerica.com";
            message.Subject = "ERROR - MKFinance Deposit File";
            message.Message = MessageBody.ToString();

            email.SendMCAKFormattedEmail(message);

            email.Close();

        }

        static void sendErrorEmail(string errorMessage)
        {
            
            
            NotifyService.NotifyServiceClient email = new NotifyService.NotifyServiceClient();
            email.Open();

            StringBuilder MessageBody = new StringBuilder();
            MessageBody.Append("<table width='600' cellpadding='2'>");
            MessageBody.Append("<tr>");
            MessageBody.Append("<td class='highlightBodyHeader'>MKFinance Deposit File - ERROR</td>");
            MessageBody.Append("</tr>");
            MessageBody.Append("<tr>");
            MessageBody.Append("<td class='bodySmall'><br /><br />" + errorMessage.ToString() + "<br /><br /><br /></td>");
            MessageBody.Append("</tr>");
            MessageBody.Append("</table>");           

            NotifyService.Msg message = new NotifyService.Msg();

            message.Recipients = ConvertStringToStringList(System.Configuration.ConfigurationManager.AppSettings["TechnicalContacts"].ToString(), ';');            
            message.AppName = "MKFinance Deposit File Extract";
            message.isHTML = true;
            message.Sender = "error@oneamerica.com";
            message.Subject = "ERROR - MKFinance Deposit File";
            message.Message = MessageBody.ToString();

            email.SendMCAKFormattedEmail(message);

            email.Close();

        }

        static void sendConfirmationEmail(string messagebody)
        {
            
            NotifyService.NotifyServiceClient email = new NotifyService.NotifyServiceClient();
            email.Open();
            NotifyService.Msg message = new NotifyService.Msg();
            message.Recipients = ConvertStringToStringList(System.Configuration.ConfigurationManager.AppSettings["TechnicalContacts"].ToString(), ';');
            message.AppName = "MKFinance Deposit File Extract";
            message.Sender = "notify@oneamerica.com";
            message.Subject = "MKFinance Deposit File";
            message.Message = messagebody;

            email.SendMCAKFormattedEmail(message);
            email.Close();
        }

        public static List<string> ConvertStringToStringList(string items, char delimiter)
        {
            return items.Split(';').ToList();
        }
    }

}
