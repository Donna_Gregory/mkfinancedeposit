﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.269
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MKFinanceDeposit.NotifyService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Msg", Namespace="http://schemas.datacontract.org/2004/07/")]
    [System.SerializableAttribute()]
    public partial class Msg : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AppNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Collections.Generic.List<string> BCCRecipientsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Collections.Generic.List<string> CCRecipientsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string HeaderField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Collections.Generic.List<string> RecipientsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SenderField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SubjectField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool isHTMLField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AppName {
            get {
                return this.AppNameField;
            }
            set {
                if ((object.ReferenceEquals(this.AppNameField, value) != true)) {
                    this.AppNameField = value;
                    this.RaisePropertyChanged("AppName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Collections.Generic.List<string> BCCRecipients {
            get {
                return this.BCCRecipientsField;
            }
            set {
                if ((object.ReferenceEquals(this.BCCRecipientsField, value) != true)) {
                    this.BCCRecipientsField = value;
                    this.RaisePropertyChanged("BCCRecipients");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Collections.Generic.List<string> CCRecipients {
            get {
                return this.CCRecipientsField;
            }
            set {
                if ((object.ReferenceEquals(this.CCRecipientsField, value) != true)) {
                    this.CCRecipientsField = value;
                    this.RaisePropertyChanged("CCRecipients");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Header {
            get {
                return this.HeaderField;
            }
            set {
                if ((object.ReferenceEquals(this.HeaderField, value) != true)) {
                    this.HeaderField = value;
                    this.RaisePropertyChanged("Header");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Collections.Generic.List<string> Recipients {
            get {
                return this.RecipientsField;
            }
            set {
                if ((object.ReferenceEquals(this.RecipientsField, value) != true)) {
                    this.RecipientsField = value;
                    this.RaisePropertyChanged("Recipients");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Sender {
            get {
                return this.SenderField;
            }
            set {
                if ((object.ReferenceEquals(this.SenderField, value) != true)) {
                    this.SenderField = value;
                    this.RaisePropertyChanged("Sender");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Subject {
            get {
                return this.SubjectField;
            }
            set {
                if ((object.ReferenceEquals(this.SubjectField, value) != true)) {
                    this.SubjectField = value;
                    this.RaisePropertyChanged("Subject");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool isHTML {
            get {
                return this.isHTMLField;
            }
            set {
                if ((this.isHTMLField.Equals(value) != true)) {
                    this.isHTMLField = value;
                    this.RaisePropertyChanged("isHTML");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="NotifyService.INotifyService")]
    public interface INotifyService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INotifyService/SendMSafeFormattedEmail", ReplyAction="http://tempuri.org/INotifyService/SendMSafeFormattedEmailResponse")]
        int SendMSafeFormattedEmail(MKFinanceDeposit.NotifyService.Msg composite);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INotifyService/SendMCAKFormattedEmail", ReplyAction="http://tempuri.org/INotifyService/SendMCAKFormattedEmailResponse")]
        int SendMCAKFormattedEmail(MKFinanceDeposit.NotifyService.Msg composite);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INotifyService/SendEmail", ReplyAction="http://tempuri.org/INotifyService/SendEmailResponse")]
        int SendEmail(MKFinanceDeposit.NotifyService.Msg composite);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INotifyService/ReSendEmail", ReplyAction="http://tempuri.org/INotifyService/ReSendEmailResponse")]
        void ReSendEmail(int MessageID);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface INotifyServiceChannel : MKFinanceDeposit.NotifyService.INotifyService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class NotifyServiceClient : System.ServiceModel.ClientBase<MKFinanceDeposit.NotifyService.INotifyService>, MKFinanceDeposit.NotifyService.INotifyService {
        
        public NotifyServiceClient() {
        }
        
        public NotifyServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public NotifyServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public NotifyServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public NotifyServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int SendMSafeFormattedEmail(MKFinanceDeposit.NotifyService.Msg composite) {
            return base.Channel.SendMSafeFormattedEmail(composite);
        }
        
        public int SendMCAKFormattedEmail(MKFinanceDeposit.NotifyService.Msg composite) {
            return base.Channel.SendMCAKFormattedEmail(composite);
        }
        
        public int SendEmail(MKFinanceDeposit.NotifyService.Msg composite) {
            return base.Channel.SendEmail(composite);
        }
        
        public void ReSendEmail(int MessageID) {
            base.Channel.ReSendEmail(MessageID);
        }
    }
}
